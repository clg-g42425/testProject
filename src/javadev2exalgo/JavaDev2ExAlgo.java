/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadev2exalgo;

import java.util.*;

/**
 *
 * @author G42425
 */



public class JavaDev2ExAlgo {

    /**
     *
     * @param demText
     * @return
     */
    public static int demandeInt(String demText){
        Scanner clavier = new Scanner(System.in);
        System.out.print("" + demText);
        int dem = clavier.nextInt();
        return dem;
    }
    
    public static String demandeString(String demText){
        Scanner clavier = new Scanner(System.in);
        System.out.print("" + demText);
        String dem = clavier.nextLine();
        return dem;
    }
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<String> list = new ArrayList<>();
        ArrayList<Integer> listInt = new ArrayList<>();

        int demande;
        int demandeJour;
        int demandeAnnée;
        int demandeMois;
        String demandeNom;
        String demandePrenom;
        int nombreDePersonne;
        int sep = 0;
        int sec = 0;
        int sech =0;
        
        nombreDePersonne = demandeInt("Combien de personne y-a-t-il : ");
        for(int i = 0; i < nombreDePersonne; i++){
            demandeNom = demandeString("Quelle est son nom : ");
            list.add(demandeNom);
            demandePrenom = demandeString("Quelle est son prénom : ");
            list.add(demandePrenom);
            demandeJour = demandeInt("Quelle jour est-il né : ");
            listInt.add(demandeJour);
            demandeMois = demandeInt("Quelle mois (en chiffre) : ");
            listInt.add(demandeMois);
            demandeAnnée = demandeInt("Quelle année : ");
            listInt.add(demandeAnnée);
        }
        demande = demandeInt("Veillez indiquer le mois que vous voullez afficher : ");
        
        for (int i = 1 ; i <listInt.size(); i=3+i) {
            int position = listInt.get(i);
            sep = sep + 1;
            if(position == demande){
                sec = i-sep;
                sech = sec + 1;
                for(int j = sec; j <= sech;j++){
                    String maValeur = list.get(j);
                    System.out.print(maValeur + " ");
                }
                sec = i-1;
                sech = i+1;
                for(int k = sec; k <= sech;k++){
                    int maValeur2 = listInt.get(k);
                    System.out.print(maValeur2 + " ");
                }
                System.out.println("");
            }
        }
    }
    
}
